#!/bin/bash

if [ -n "$1" ]; then
    mkdir "$1"
    cd "$1"
    ROOT_DIR=$(pwd)
    git clone https://emprovements@bitbucket.org/emprovements/bbb.git
    git clone -b 2015.11 --depth 1 git://git.buildroot.net/buildroot
    cd buildroot
    make BR2_EXTERNAL=../bbb bbb_defconfig
    time make 2>&1 | tee build.log
else
    echo "Specify name of the folder to build buildroot framework ..."
fi
